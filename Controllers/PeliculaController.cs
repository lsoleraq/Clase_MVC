﻿using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ClaseMVC.Models;
using PagedList;

namespace ClaseMVC.Controllers
{
    public class PeliculaController : Controller
    {
        private PeliculaDBContext db = new PeliculaDBContext();

        //
        // GET: /Pelicula/

        public ActionResult Index(int pagina = 1)
        {
            return View(db.peliculas.ToList().ToPagedList(pagina, 18));
        }


        public ActionResult rating()
        {
            return View(db.peliculas.ToList());
        }

        //
        // GET: /Pelicula/Details/5

        public ActionResult Details(int id = 0)
        {
            Pelicula pelicula = db.peliculas.Find(id);
            if (pelicula == null)
            {
                return HttpNotFound();
            }
            return View(pelicula);
        }

        //
        // GET: /Pelicula/Create
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Pelicula/Create
    
        [HttpPost]
        public ActionResult Create(Pelicula pelicula, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                if (file != null && file.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(file.FileName);
                    var path = Path.Combine(
                        Server.MapPath("~/Content/imagenes"),
                        fileName);
                    file.SaveAs(path);
                    //path = Url.Content(Path.Combine("~/content/imagenes", fileName));
                    path = "/content/imagenes/" + fileName;
                    pelicula.ruta = path;
                }
                db.peliculas.Add(pelicula);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(pelicula);
        }

        //
        // GET: /Pelicula/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Pelicula pelicula = db.peliculas.Find(id);
            if (pelicula == null)
            {
                return HttpNotFound();
            }
            return View(pelicula);
        }

        //
        // POST: /Pelicula/Edit/5
        [HttpPost]
        public ActionResult Edit(Pelicula pelicula, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                if (file != null && file.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(file.FileName);
                    var path = Path.Combine(
                        Server.MapPath("~/Content/imagenes"),
                        fileName);
                    file.SaveAs(path);
                    //path = Url.Content(Path.Combine("~/content/imagenes", fileName));
                    path = "/content/imagenes/" + fileName;
                    pelicula.ruta = path;
                }
                db.Entry(pelicula).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(pelicula);
        }
       
        //
        // GET: /Pelicula/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Pelicula pelicula = db.peliculas.Find(id);
            if (pelicula == null)
            {
                return HttpNotFound();
            }
            return View(pelicula);
        }

        //
        // POST: /Pelicula/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Pelicula pelicula = db.peliculas.Find(id);
            db.peliculas.Remove(pelicula);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult RateProduct(int id, int rate)
        {
            int userId = 142; // WebSecurity.CurrentUserId;
            bool success = false;
            string error = "";

            try
            {
                success = CalificarPelicula(userId, id, rate);
            }
            catch (System.Exception ex)
            {
                // get last error
                if (ex.InnerException != null)
                    while (ex.InnerException != null)
                        ex = ex.InnerException;

                error = ex.Message;
            }

            return Json(new { error = error, success = success, pid = id }, JsonRequestBehavior.AllowGet);
        }

        public bool CalificarPelicula(int userId, int id, int calificacion)
        {
            Pelicula pelicula = db.peliculas.Find(id);
            if (pelicula == null)
            {
                return false;
            }
            //pelicula.Rate = calificacion;
            db.Entry(pelicula).State = EntityState.Modified;
            db.SaveChanges();
            return true;
        }


        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        public ActionResult llamarAPI()
        {

            return View();
        }
    }
}