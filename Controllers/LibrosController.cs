﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ClaseMVC.Models;
using PagedList;

namespace ClaseMVC.Controllers
{
    public class LibrosController : Controller
    {
        private LibroDBContext db = new LibroDBContext();

        //
        // GET: /Libros/

        public ActionResult Index(int pagina =1)
        {
            return View(db.libros.ToList().ToPagedList(pagina, 10));
        }

        //
        // GET: /Libros/Details/5

        public ActionResult Details(int id = 0)
        {
            Libro libro = db.libros.Find(id);
            if (libro == null)
            {
                return HttpNotFound();
            }
            return View(libro);
        }

        //
        // GET: /Libros/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Libros/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Libro libro)
        {
            if (ModelState.IsValid)
            {
                db.libros.Add(libro);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(libro);
        }

        //
        // GET: /Libros/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Libro libro = db.libros.Find(id);
            if (libro == null)
            {
                return HttpNotFound();
            }
            return View(libro);
        }

        //
        // POST: /Libros/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Libro libro)
        {
            if (ModelState.IsValid)
            {
                db.Entry(libro).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(libro);
        }

        //
        // GET: /Libros/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Libro libro = db.libros.Find(id);
            if (libro == null)
            {
                return HttpNotFound();
            }
            return View(libro);
        }

        //
        // POST: /Libros/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Libro libro = db.libros.Find(id);
            db.libros.Remove(libro);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}