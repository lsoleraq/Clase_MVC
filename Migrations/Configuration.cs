using ClaseMVC.Models;
using System.Web.Security;
using WebMatrix.WebData;
using System.Linq;
using System;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;

namespace ClaseMVC.Migrations
{  

    internal sealed class Configuration : DbMigrationsConfiguration<ClaseMVC.Models.PeliculaDBContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
            ContextKey = "ClaseMVC.Models.PeliculaDBContext";
        }

        protected override void Seed(ClaseMVC.Models.PeliculaDBContext context)
        {
            Random rnd = new Random(); 
            for (int index = 0; index < 100; index++)
            {
                context.peliculas.AddOrUpdate(i => i.Titulo,
                    new Pelicula
                    {
                        Titulo = "jurassik park " + index.ToString(),
                        Fecha = DateTime.Parse("1989-1-11"),
                        ActorPrincipal = "T-Rex",
                        precio = 7,
                        ruta = "/Content/imagenes/HP" +
                            rnd.Next(1, 5).ToString() + ".jpg"

                    }
               );
            }

            SeedMembership();

        }

        private void SeedMembership()
        {
            WebSecurity.InitializeDatabaseConnection("DefaultConnection",
                "UserProfile", "UserId", "UserName", autoCreateTables: true);

            var roles = (SimpleRoleProvider)Roles.Provider;
            var membership = (SimpleMembershipProvider)Membership.Provider;

            if (!roles.RoleExists("Admin"))
            {
                roles.CreateRole("Admin");
            }
            if (membership.GetUser("spuc", false) == null)
            {
                membership.CreateUserAndAccount("spuc", "password");
            }
            if (!roles.GetRolesForUser("spuc").Contains("Admin"))
            {
                roles.AddUsersToRoles(new[] { "spuc" }, new[] { "admin" });
            }
        }

    }
}
