using System.Web.Security;
using ClaseMVC.Models;

namespace ClaseMVC.Migrations.LibroDBContext
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using WebMatrix.WebData;

    internal sealed class Configuration : DbMigrationsConfiguration<ClaseMVC.Models.LibroDBContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            MigrationsDirectory = @"Migrations\LibroDBContext";
        }

        protected override void Seed(ClaseMVC.Models.LibroDBContext context)
        {
            Random rnd = new Random();
            #region Libros
            for (int index = 0; index < 100; index++)
            {
                context.libros.AddOrUpdate(i => i.Titulo,
                    new Libro()
                    {
                        Titulo = "Harry Potter" + index.ToString(),
                        Genero = "Ficcion",
                        precio = 7,
                        Autor = "J K Rowling"
                    }
               );
            }
            #endregion

            SeedMembership();

        }

        private void SeedMembership()
        {
            WebSecurity.InitializeDatabaseConnection("DefaultConnection",
                "UserProfile", "UserId", "UserName", autoCreateTables: true);

            var roles = (SimpleRoleProvider)Roles.Provider;
            var membership = (SimpleMembershipProvider)Membership.Provider;

            if (!roles.RoleExists("Admin"))
            {
                roles.CreateRole("Admin");
            }
            if (membership.GetUser("spuc", false) == null)
            {
                membership.CreateUserAndAccount("spuc", "password");
            }
            if (!roles.GetRolesForUser("spuc").Contains("Admin"))
            {
                roles.AddUsersToRoles(new[] { "spuc" }, new[] { "admin" });
            }
        }
    }
}
