namespace ClaseMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Comentarios",
                c => new
                    {
                        ComentarioID = c.Int(nullable: false, identity: true),
                        PeliculaID = c.Int(nullable: false),
                        usuario = c.String(),
                        Tema = c.String(nullable: false, maxLength: 250),
                        Texto = c.String(),
                    })
                .PrimaryKey(t => t.ComentarioID)
                .ForeignKey("dbo.Peliculas", t => t.PeliculaID, cascadeDelete: true)
                .Index(t => t.PeliculaID);
            
            CreateTable(
                "dbo.Peliculas",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        Titulo = c.String(nullable: false, maxLength: 100),
                        ActorPrincipal = c.String(nullable: false),
                        Fecha = c.DateTime(nullable: false),
                        precio = c.Decimal(nullable: false, precision: 18, scale: 2),
                        categoria = c.Int(),
                        imagen = c.Binary(),
                        ImageMimeType = c.String(),
                        Rate = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Comentarios", "PeliculaID", "dbo.Peliculas");
            DropIndex("dbo.Comentarios", new[] { "PeliculaID" });
            DropTable("dbo.Peliculas");
            DropTable("dbo.Comentarios");
        }
    }
}
