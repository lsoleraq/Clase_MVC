﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClaseMVC.Models
{
    public enum Categoria
    {
        SciFi,
        [Display(Name = "Ciencia Ficcion")]
        Drama,
        [Display(Name = "Comedia")]
        Comedia
    }

    public class Pelicula
    {
        //public Pelicula()
        //{
        //    this.Rate = this.id = 0;
        //}

        //public int Rate { get; set; }

        public int id { get; set; }
        [Required(ErrorMessage="*")]
        [StringLength(100, MinimumLength = 5, ErrorMessage = "El {0} debe ser de al menos {2} caracteres.")]
        public string Titulo { get; set; }
        [Required(ErrorMessage = "*")]
        public string ActorPrincipal { get; set; }
               
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}",
            ApplyFormatInEditMode = true)]
        public DateTime Fecha { get; set; }

        public decimal precio { get; set; }

        public Categoria? categoria { get; set; }

        public string ruta { get; set; }

        
    }

    public class PeliculaDBContext : DbContext
    {
        public PeliculaDBContext()
            : base("PeliculaConnection")
        {
        }

        public DbSet<Pelicula> peliculas { get; set; }
    }
}